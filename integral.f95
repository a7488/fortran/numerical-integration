module integral
implicit none

contains

   function f(x); real(8) f,x; f=x; end function f


   function trap(f,a,b,n); real(8) trap, a, b, h, x, s; integer n, i; real(8), external :: f
	h=(b-a)/n; s=(f(a)+f(b))/2
do i=2,n
   x=a+(i-1)*h;s=s+f(x)
enddo
trap=s*h
   end function trap


   function rectan(f,a,b,n); real(8) rectan, a, b, x, h, s; integer n, k; real(8), external :: f
h=(b-a)/n; s=0.
do k=1,n
   x=a+(k-0.5)*h; s=s+f(x)
enddo
rectan=s*h
   end function rectan


   function sim(f,a,b,n); real(8) sim, a, b, h, x, s; integer n, k; real(8), external :: f
h=(b-a)/n; s=(f(a)+f(b))
do k=2,n
   x=a+(k-1)*h
   if(mod(k,2)==0) then; s=s+4*f(x); else; s=s+2*f(x);endif
enddo
sim=h*s/3.
end function sim


end module integral
