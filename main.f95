program main
use integral
implicit none
real(8) a,b,rest,resr,ress
integer n
read (*,*) a, b, n

rest=trap(f,a,b,n); resr=rectan(f,a,b,n); ress=sim(f,a,b,n)
write(*,*) rest, resr, ress

end program main
